<?php
/**
 * @file
 * Template file for the symphony Event display page.
 *
 * Variables available:
 * $event: The event array (the SOAP response transformed into an array)
 *
 * If the $event contains the 'WebsitePageContent', we render this instead
 * of rendering the field values specifically. i.e. WebsitePageContent
 * overrides the event detail page content (except for the booking link and
 * content block at the bottom)
 *
 */
?>
<?php if (!empty($event['WebsitePageContent'])): ?>
  <?php print $event['WebsitePageContent']; ?>

<?php else: ?>
  <?php if (!empty($event['ImageThumbLink'])): ?>
    <div style='width: 45%;float: right;'>
      <img width='100%' alt='Event image' src='<?php print $event['ImageThumbLink']; ?>' /><br />
    </div>
  <?php endif; ?>

  <?php if (!empty($event['LongDescription'])): ?>
    <?php print $event['LongDescription']; ?>
  <?php elseif (!empty($event['MediumDescription'])): ?>
    <?php print $event['MediumDescription']; ?>
  <?php elseif (!empty($event['Description'])): ?>
    <?php print $event['Description']; ?>
  <?php endif; ?>

<?php endif; ?>

<?php if ($event['DefaultBookingFormUrl'] != '{NO DEFAULT FORM SELECTED}'): ?>
  <p><a class='button' target='_blank' href='<?php print 'http://'.$event['DefaultBookingFormUrl']; ?>'>
    Book Now</a></p>
<?php endif; ?>

<div class="region region-sidebar-second">
  <div id="block-shl-shared-event-details" class="block block-shl-shared">
    <h2>Event details</h2>
    <div class="content">
      <div class="item-list">
        <ul class="event-details icon-list">
          <?php if (!empty($event['StartDate'])): ?>
            <li class="event-date first">
              <?php print format_date(strtotime($event['StartDate']), 'custom', 'd F Y H:i'); ?>
            </li>
          <?php endif; ?>

          <?php if (!empty($event['Location'])): ?>
            <li class="event-location"><?php print $event['Location']; ?></li>
          <?php elseif (!empty($event['Code_Room'])): ?>
            <li class="event-location"><?php print $event['Code_Room']; ?></li>
          <?php endif; ?>

          <?php if (!empty($event['Code_Series'])): ?>
            <li class="event-type"><?php print $event['Code_Series']; ?></li>
          <?php endif; ?>
          <?php if (!empty($event['Code_Type'])): ?>
            <li class="event-type"><?php print $event['Code_Type']; ?></li>
          <?php endif; ?>
          <?php if (!empty($event['institute_link']) && empty($event['institute_link']['#printed'])): ?>
            <li class="event-location event-type-senate-house last">
              <?php print drupal_render($event['institute_link']); ?>
            </li>
          <?php endif; ?>
        </ul>
      </div>
    </div>
  </div>

  <div id="block-block-2" class="block block-block">
    <h2>Need help?</h2>
    <div class="content">
      <ul class="icon-list">
        <li><a class="contact" href="/about-us/contact-us">Contact us</a></li>
        <li><a class="feedback" href="/using-the-library/tell-us-what-you-think">Leave us suggestions and feedback</a></li>
        <li><a class="mailing-list" href="/about-us/mailing-list">Sign up to our mailing list</a></li>
      </ul>
    </div>
  </div>
</div>
